# UU Nutria

Welcome to my homework assigment repo. My name is Marek Ostrihon
and this my semester project for subject **Cloud Application Architecture** at **Unicorn University** in summer 2023/24

The project is a simple web app for counting calorie intake.
It consists of BE and FE application.

BE is written in `Express.js` and can be found in folder `server`\
FE is written in `React.js` and can be found in folder `client`

## Getting started

### Backend application

In order to run the BE server it is required to have `node.js` installed on your machine

1) Navigate to BE source folder `server`

```
cd ./server
```

2) Install dependencies:

```
npm install
```

3) Start up the server:

```
npm start
```

The server will be listening on port 8000

---
For a quick check go to your browser and open this address:

```
http://localhost:8000/health
```

You should see the following text:
```
I am alive!
```

For documentation of other endpoints please see the Postman collection:

```
./server/doc/UU_Nutria_BE.postman_collection.json
```

### Frontend application

![Food Items](images/food_items.png)

![Adding a food item](images/add_food_item.png)

![Meal Plans](images/meal_plans.png)

![Adding a meal plan](images/add_meal_plan.png)

![Adding a meal plan record #1](images/add_meal_plan_record_1.png)

![Adding a meal plan record #2](images/add_meal_plan_record_2.png)