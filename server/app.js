const express = require("express");
const cors = require("cors");
const app = express();
const port = 8000;

const foodItemController = require("./controller/foodItem");
const mealPlanController = require("./controller/mealPlan");
const mealPlanRecordController = require("./controller/mealPlanRecord");

app.use(express.json()); // Support for application/json
app.use(express.urlencoded({ extended: true })); // Support for application/x-www-form-urlencoded

app.use(cors());

// Health endpoint
app.get("/health", (req, res) => {
  res.send("I am alive!");
});

// Routing
app.use("/foodItem", foodItemController);
app.use("/mealPlan", mealPlanController);
app.use("/mealPlanRecord", mealPlanRecordController);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
