const express = require("express");
const router = express.Router();

const ListAbl = require("../abl/mealPlan/listAbl");
const CreateAbl = require("../abl/mealPlan/CreateAbl");

router.get("/list", ListAbl);
router.post("/create", CreateAbl);

module.exports = router;
