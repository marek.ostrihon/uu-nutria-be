const express = require("express");
const router = express.Router();

const CreateAbl = require("../abl/mealPlanRecord/CreateAbl");

router.post("/create", CreateAbl);

module.exports = router;
