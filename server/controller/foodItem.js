const express = require("express");
const router = express.Router();

const ListAbl = require("../abl/foodItem/listAbl");
const CreateAbl = require("../abl/foodItem/CreateAbl");

router.get("/list", ListAbl);
router.post("/create", CreateAbl);

module.exports = router;
