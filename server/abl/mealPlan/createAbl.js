const Ajv = require("ajv");
const ajv = new Ajv();
const validateDateTime = require("../../helpers/validate-date-time.js");
ajv.addFormat("date-time", { validate: validateDateTime });

const mealPlanDao = require("../../dao/mealPlan-dao.js");

const schema = {
  type: "object",
  properties: {
    date: { type: "string", format: "date-time" },
    calorie_goal: { type: "integer"},
  },
  required: ["date", "calorie_goal"],
  additionalProperties: false,
};

async function CreateAbl(req, res) {
  try {
    let mealPlan = req.body;

    // validate input
    const valid = ajv.validate(schema, mealPlan);
    if (!valid) {
      res.status(400).json({
        code: "dtoInIsNotValid",
        message: "dtoIn is not valid",
        validationError: ajv.errors,
      });
      return;
    }

    mealPlan = mealPlanDao.create(mealPlan);
    res.json(mealPlan);
  } catch (e) {
    res.status(500).json({ message: e.message });
  }
}

module.exports = CreateAbl;
