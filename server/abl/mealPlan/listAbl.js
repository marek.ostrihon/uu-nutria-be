const mealPlanDao = require("../../dao/mealPlan-dao.js");
const foodItemDao = require("../../dao/foodItem-dao.js");
const mealPlanRecordDao = require("../../dao/mealPlanRecord-dao.js");

async function ListAbl(req, res) {
  try {
    const mealPlanList = mealPlanDao.list();

    const mealPlanRecordList = mealPlanRecordDao.list();

    // Create object with meal plan records per plan
    const mealPlanRecordsByMealPlan = {};
    mealPlanRecordList.forEach(
        (mealPlanRecord) => {
          if (!mealPlanRecordsByMealPlan[mealPlanRecord.meal_plan_id]) {
              mealPlanRecordsByMealPlan[mealPlanRecord.meal_plan_id] = []
            }

          const foodItem = foodItemDao.get(mealPlanRecord.food_item_id)
          mealPlanRecord.food_item_name = foodItem.name
          mealPlanRecord.food_item_energy_value = foodItem.energy_value

          mealPlanRecordsByMealPlan[mealPlanRecord.meal_plan_id].push(mealPlanRecord)
        }
    )

    // Add corresponding meal plan records to each meal plan
    mealPlanList.forEach(
        (mealPlan) => mealPlan['meal_plan_records'] = mealPlanRecordsByMealPlan[mealPlan.id] || []
    )

    res.json(mealPlanList);
  } catch (e) {
    res.status(500).json({ message: e.message });
  }
}

module.exports = ListAbl;
