const Ajv = require("ajv");
const addFormats = require("ajv-formats").default;
const ajv = new Ajv();
addFormats(ajv);

const foodItemDao = require("../../dao/foodItem-dao.js");

const schema = {
  type: "object",
  properties: {
    name: { type: "string" },
    energy_value: { type: "integer" },
  },
  required: ["name", "energy_value"],
  additionalProperties: false,
};

async function CreateAbl(req, res) {
  try {
    let foodItem = req.body;

    // validate input
    const valid = ajv.validate(schema, foodItem);
    if (!valid) {
      res.status(400).json({
        code: "dtoInIsNotValid",
        message: "dtoIn is not valid",
        validationError: ajv.errors,
      });
      return;
    }

    foodItem = foodItemDao.create(foodItem);
    res.json(foodItem);
  } catch (e) {
    res.status(500).json({ message: e.message });
  }
}

module.exports = CreateAbl;
