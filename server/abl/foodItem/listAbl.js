const foodItemDao = require("../../dao/foodItem-dao.js");

async function ListAbl(req, res) {
  try {
    const foodItemList = foodItemDao.list();
    res.json(foodItemList);
  } catch (e) {
    res.status(500).json({ message: e.message });
  }
}

module.exports = ListAbl;
