const Ajv = require("ajv");
const ajv = new Ajv();
const validateDateTime = require("../../helpers/validate-date-time.js");
ajv.addFormat("date-time", { validate: validateDateTime });

const foodItemDao = require("../../dao/foodItem-dao.js");
const mealPlanDao = require("../../dao/mealPlan-dao.js");
const mealPlanRecordDao = require("../../dao/mealPlanRecord-dao.js");

const schema = {
  type: "object",
  properties: {
    meal_plan_id: { type: "string" },
    food_item_id: { type: "string" },
    weight: { type: "integer" },
  },
  required: ["meal_plan_id", "food_item_id", "weight"],
  additionalProperties: false
};

async function CreateAbl(req, res) {
  try {
    let mealPlanRecord = req.body;

    // validate input
    const valid = ajv.validate(schema, mealPlanRecord);
    if (!valid) {
      res.status(400).json({
        code: "dtoInIsNotValid",
        message: "dtoIn is not valid",
        validationError: ajv.errors,
      });
      return;
    }

    // Check whether meal plan exists
    const mealPlanList = mealPlanDao.list();
    const mealPlanExists = mealPlanList.some((mp) => mp.id === mealPlanRecord.meal_plan_id);
    if (!mealPlanExists) {
      res.status(400).json({
        code: "mealPlanNotFound",
        message: `Meal plan ${mealPlanRecord.meal_plan_id} does not exist.`,
      });
      return;
    }

    // Check whether food item exists
    const foodItemList = foodItemDao.list();
    const foodItemExists = foodItemList.some((fi) => fi.id === mealPlanRecord.food_item_id);
    if (!foodItemExists) {
      res.status(400).json({
        code: "foodItemNotFound",
        message: `Food item ${mealPlanRecord.food_item_id} does not exist.`,
      });
      return;
    }


    mealPlanRecord = mealPlanRecordDao.create(mealPlanRecord);
    res.json(mealPlanRecord);
  } catch (e) {
    res.status(500).json({ message: e.message });
  }
}

module.exports = CreateAbl;
