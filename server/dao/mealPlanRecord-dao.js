const fs = require("fs");
const path = require("path");
const crypto = require("crypto");

const mealPlanRecordFolderPath = path.join(__dirname, "storage", "mealPlanRecordList");

// Method to read a mealPlanRecord from a file
function get(mealPlanRecordId) {
  try {
    const filePath = path.join(mealPlanRecordFolderPath, `${mealPlanRecordId}.json`);
    const fileData = fs.readFileSync(filePath, "utf8");
    return JSON.parse(fileData);
  } catch (error) {
    if (error.code === "ENOENT") return null;
    throw { code: "failedToReadMealPlanRecord", message: error.message };
  }
}

// Method to write a mealPlanRecord to a file
function create(mealPlanRecord) {
  try {
    mealPlanRecord.id = crypto.randomBytes(16).toString("hex");
    const filePath = path.join(mealPlanRecordFolderPath, `${mealPlanRecord.id}.json`);
    const fileData = JSON.stringify(mealPlanRecord);
    fs.writeFileSync(filePath, fileData, "utf8");
    return mealPlanRecord;
  } catch (error) {
    throw { code: "failedToCreateMealPlanRecord", message: error.message };
  }
}

// Method to update a mealPlanRecord in a file
function update(mealPlanRecord) {
  try {
    const currentMealPlanRecord = get(mealPlanRecord.id);
    if (!currentMealPlanRecord) return null;
    const newMealPlanRecord = { ...currentMealPlanRecord, ...mealPlanRecord };
    const filePath = path.join(mealPlanRecordFolderPath, `${mealPlanRecord.id}.json`);
    const fileData = JSON.stringify(newMealPlanRecord);
    fs.writeFileSync(filePath, fileData, "utf8");
    return newMealPlanRecord;
  } catch (error) {
    throw { code: "failedToUpdateMealPlanRecord", message: error.message };
  }
}

// Method to remove a mealPlanRecord from a file
function remove(mealPlanRecordId) {
  try {
    const filePath = path.join(mealPlanRecordFolderPath, `${mealPlanRecordId}.json`);
    fs.unlinkSync(filePath);
    return {};
  } catch (error) {
    if (error.code === "ENOENT") {
      return {};
    }
    throw { code: "failedToRemoveMealPlanRecord", message: error.message };
  }
}

// Method to list mealPlanRecords in a folder
function list() {
  try {
    const files = fs.readdirSync(mealPlanRecordFolderPath);
    const mealPlanRecordList = files.map((file) => {
      const fileData = fs.readFileSync(path.join(mealPlanRecordFolderPath, file), "utf8");
      return JSON.parse(fileData);
    });
    return mealPlanRecordList;
  } catch (error) {
    throw { code: "failedToListMealPlanRecords", message: error.message };
  }
}

module.exports = {
  get,
  create,
  update,
  remove,
  list,
};
