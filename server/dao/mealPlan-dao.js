const fs = require("fs");
const path = require("path");
const crypto = require("crypto");

const mealPlanFolderPath = path.join(__dirname, "storage", "mealPlanList");

// Method to read a mealPlan from a file
function get(mealPlanId) {
  try {
    const filePath = path.join(mealPlanFolderPath, `${mealPlanId}.json`);
    const fileData = fs.readFileSync(filePath, "utf8");
    return JSON.parse(fileData);
  } catch (error) {
    if (error.code === "ENOENT") return null;
    throw { code: "failedToReadMealPlan", message: error.message };
  }
}

// Method to write a mealPlan to a file
function create(mealPlan) {
  try {
    mealPlan.id = crypto.randomBytes(16).toString("hex");
    const filePath = path.join(mealPlanFolderPath, `${mealPlan.id}.json`);
    const fileData = JSON.stringify(mealPlan);
    fs.writeFileSync(filePath, fileData, "utf8");
    return mealPlan;
  } catch (error) {
    throw { code: "failedToCreateMealPlan", message: error.message };
  }
}

// Method to update a mealPlan in a file
function update(mealPlan) {
  try {
    const currentMealPlan = get(mealPlan.id);
    if (!currentMealPlan) return null;
    const newMealPlan = { ...currentMealPlan, ...mealPlan };
    const filePath = path.join(mealPlanFolderPath, `${mealPlan.id}.json`);
    const fileData = JSON.stringify(newMealPlan);
    fs.writeFileSync(filePath, fileData, "utf8");
    return newMealPlan;
  } catch (error) {
    throw { code: "failedToUpdateMealPlan", message: error.message };
  }
}

// Method to remove a mealPlan from a file
function remove(mealPlanId) {
  try {
    const filePath = path.join(mealPlanFolderPath, `${mealPlanId}.json`);
    fs.unlinkSync(filePath);
    return {};
  } catch (error) {
    if (error.code === "ENOENT") {
      return {};
    }
    throw { code: "failedToRemoveMealPlan", message: error.message };
  }
}

// Method to list mealPlans in a folder
function list() {
  try {
    const files = fs.readdirSync(mealPlanFolderPath);
    const mealPlanList = files.map((file) => {
      const fileData = fs.readFileSync(path.join(mealPlanFolderPath, file), "utf8");
      return JSON.parse(fileData);
    });
    return mealPlanList;
  } catch (error) {
    throw { code: "failedToListMealPlans", message: error.message };
  }
}

module.exports = {
  get,
  create,
  update,
  remove,
  list,
};
