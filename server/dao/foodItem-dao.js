const fs = require("fs");
const path = require("path");
const crypto = require("crypto");

const foodItemFolderPath = path.join(__dirname, "storage", "foodItemList");

// Method to read a foodItem from a file
function get(foodItemId) {
  try {
    const filePath = path.join(foodItemFolderPath, `${foodItemId}.json`);
    const fileData = fs.readFileSync(filePath, "utf8");
    return JSON.parse(fileData);
  } catch (error) {
    if (error.code === "ENOENT") return null;
    throw { code: "failedToReadFoodItem", message: error.message };
  }
}

// Method to write a foodItem to a file
function create(foodItem) {
  try {
    foodItem.id = crypto.randomBytes(16).toString("hex");
    const filePath = path.join(foodItemFolderPath, `${foodItem.id}.json`);
    const fileData = JSON.stringify(foodItem);
    fs.writeFileSync(filePath, fileData, "utf8");
    return foodItem;
  } catch (error) {
    throw { code: "failedToCreateFoodItem", message: error.message };
  }
}

// Method to update a foodItem in a file
function update(foodItem) {
  try {
    const currentFoodItem = get(foodItem.id);
    if (!currentFoodItem) return null;
    const newFoodItem = { ...currentFoodItem, ...foodItem };
    const filePath = path.join(foodItemFolderPath, `${foodItem.id}.json`);
    const fileData = JSON.stringify(newFoodItem);
    fs.writeFileSync(filePath, fileData, "utf8");
    return newFoodItem;
  } catch (error) {
    throw { code: "failedToUpdateFoodItem", message: error.message };
  }
}

// Method to remove a foodItem from a file
function remove(foodItemId) {
  try {
    const filePath = path.join(foodItemFolderPath, `${foodItemId}.json`);
    fs.unlinkSync(filePath);
    return {};
  } catch (error) {
    if (error.code === "ENOENT") {
      return {};
    }
    throw { code: "failedToRemoveFoodItem", message: error.message };
  }
}

// Method to list foodItems in a folder
function list() {
  try {
    const files = fs.readdirSync(foodItemFolderPath);
    const foodItemList = files.map((file) => {
      const fileData = fs.readFileSync(path.join(foodItemFolderPath, file), "utf8");
      return JSON.parse(fileData);
    });
    return foodItemList;
  } catch (error) {
    throw { code: "failedToListFoodItems", message: error.message };
  }
}

module.exports = {
  get,
  create,
  update,
  remove,
  list,
};
