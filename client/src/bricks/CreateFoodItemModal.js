import React, {useContext, useState} from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import {mdiLoading} from "@mdi/js";
import Icon from "@mdi/react";
import FoodItemContext from "../providers/FoodItemProvider";

function CreateFoodItemModal({show, handleClose}) {
    const [validated, setValidated] = useState(false);
    const [createFoodItemCall, setCreateFoodItemCall] = useState({
        state: 'inactive'
    });
    const [formData, setFormData] = useState({
        name: "", energy_value: "",
    });


    const {handlerMap} = useContext(FoodItemContext);

    const setField = (name, val) => {
        return setFormData((formData) => {
            const newData = {...formData};
            newData[name] = val;
            return newData;
        });
    };

    const handleSubmit = async (e) => {
        const form = e.currentTarget;

        e.preventDefault();
        e.stopPropagation();

        if (!form.checkValidity()) {
            setValidated(true);
            return;
        }

        const payload = {...formData};


        try {
            setCreateFoodItemCall({state: "pending"});
            await handlerMap.handleCreateFoodItem(payload);
            handleModalClose();
            forgetData()
            setCreateFoodItemCall({state: 'inactive'})
        } catch (e) {
            console.error(e);
            setCreateFoodItemCall({state: "error", error: e.message});
        }


    };

    const forgetData = () =>{
            setFormData({name: "", energy_value: "",})
        };

    const handleModalClose = () => {
        setValidated(false);
        handleClose();
    };

    return (<Modal show={show} onHide={handleModalClose}>
            <Form noValidate validated={validated} onSubmit={handleSubmit}>
                <Modal.Header closeButton>
                    <Modal.Title>Create Food Item</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form.Group controlId="formFoodItemName">
                        <Form.Label>Food Item Name</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Enter food item name"
                            name="foodItemName"
                            value={formData.name}
                            onChange={(e) => setField("name", e.target.value)}
                            required
                        />
                        <Form.Control.Feedback type="invalid">
                            This field is required
                        </Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group controlId="formFoodItemEnergyValue">
                        <Form.Label>Food Item Energy Value</Form.Label>
                        <Form.Control
                            type="number"
                            placeholder="Enter food item energy value"
                            name="foodItemEnergyValue"
                            value={formData.energy_value}
                            onChange={(e) => setField("energy_value", parseInt(e.target.value))}
                            min={0}
                            step={10}
                            required
                        />
                        <Form.Control.Feedback type="invalid">
                            This field is required
                        </Form.Control.Feedback>
                    </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <div>
                        {createFoodItemCall.state === 'error' &&
                            <div className="text-danger">Error: {createFoodItemCall.error.errorMessage}</div>}
                    </div>
                    <Button variant="secondary" onClick={handleModalClose}>
                        Close
                    </Button>
                    <Button variant="primary" type="submit" disabled={createFoodItemCall.state === 'pending'}>
                        {createFoodItemCall.state === 'pending' ? (
                            <Icon size={0.8} path={mdiLoading} spin={true}/>) : ("Submit")}
                    </Button>
                </Modal.Footer>
            </Form>
        </Modal>);
}

export default CreateFoodItemModal;
