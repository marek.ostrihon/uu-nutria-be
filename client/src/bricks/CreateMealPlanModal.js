import React, {useContext, useState} from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import {mdiLoading} from "@mdi/js";
import Icon from "@mdi/react";
import MealPlanContext from "../providers/MealPlanProvider";

function CreateMealPlanModal({show, handleClose}) {
    const [validated, setValidated] = useState(false);
    const [createMealPlanCall, setCreateMealPlanCall] = useState({
        state: 'inactive'
    });
    const [formData, setFormData] = useState({
        date: "", calorie_goal: "",
    });


    const {handlerMap} = useContext(MealPlanContext);

    const setField = (name, val) => {
        return setFormData((formData) => {
            const newData = {...formData};
            newData[name] = val;
            return newData;
        });
    };

    const handleSubmit = async (e) => {
        const form = e.currentTarget;

        e.preventDefault();
        e.stopPropagation();

        if (!form.checkValidity()) {
            setValidated(true);
            return;
        }

        const payload = {...formData};


        try {
            setCreateMealPlanCall({state: "pending"});
            await handlerMap.handleCreateMealPlan(payload);
            handleModalClose();
            forgetData()
            setCreateMealPlanCall({state: 'inactive'})
        } catch (e) {
            console.error(e);
            setCreateMealPlanCall({state: "error", error: e.message});
        }
    };

    const forgetData = () =>{
            setFormData({date: "", calorie_goal: "",})
        };

    const handleModalClose = () => {
        setValidated(false);
        handleClose();
    };

    return (<Modal show={show} onHide={handleModalClose}>
            <Form noValidate validated={validated} onSubmit={handleSubmit}>
                <Modal.Header closeButton>
                    <Modal.Title>Create Meal Plan</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form.Group controlId="formMealPlanDate">
                        <Form.Label>Meal Plan Date</Form.Label>
                        <Form.Control
                            type="date"
                            name="mealPlanDate"
                            value={formData.date}
                            onChange={(e) => setField("date", e.target.value)}
                            required
                        />
                        <Form.Control.Feedback type="invalid">
                            This field is required
                        </Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group controlId="formMealPlanCalorieGoal">
                        <Form.Label>Meal Plan Calorie Goal</Form.Label>
                        <Form.Control
                            type="number"
                            placeholder="Enter meal plan calorie goal"
                            name="mealPlanCalorieGoal"
                            value={formData.calorie_goal}
                            onChange={(e) => setField("calorie_goal", parseInt(e.target.value))}
                            min={0}
                            step={500}
                            required
                        />
                        <Form.Control.Feedback type="invalid">
                            This field is required
                        </Form.Control.Feedback>
                    </Form.Group>
                </Modal.Body>
                <Modal.Footer>
                    <div>
                        {createMealPlanCall.state === 'error' &&
                            <div className="text-danger">Error: {createMealPlanCall.error.errorMessage}</div>}
                    </div>
                    <Button variant="secondary" onClick={handleModalClose}>
                        Close
                    </Button>
                    <Button variant="primary" type="submit" disabled={createMealPlanCall.state === 'pending'}>
                        {createMealPlanCall.state === 'pending' ? (
                            <Icon size={0.8} path={mdiLoading} spin={true}/>) : ("Submit")}
                    </Button>
                </Modal.Footer>
            </Form>
        </Modal>);
}

export default CreateMealPlanModal;
