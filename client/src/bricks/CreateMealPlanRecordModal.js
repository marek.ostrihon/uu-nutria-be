import React, {useContext, useState} from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import {mdiLoading} from "@mdi/js";
import Icon from "@mdi/react";
import MealPlanContext from "../providers/MealPlanProvider";
import FoodItemContext from "../providers/FoodItemProvider";

function CreateMealPlanRecordModal({show, handleClose, mealPlanId}) {
    const [validated, setValidated] = useState(false);
    const [createMealPlanRecordCall, setCreateMealPlanRecordCall] = useState({
        state: 'inactive'
    });
    const [formData, setFormData] = useState({
        meal_plan_id: mealPlanId, food_item_id: "", weight: "",
    });

    const {handlerMap} = useContext(MealPlanContext);
    const {data: foodItemList} = useContext(FoodItemContext);

    const setField = (name, val) => {
        return setFormData((formData) => {
            const newData = {...formData};
            newData[name] = val;
            return newData;
        });
    };

    const handleSubmit = async (e) => {
        const form = e.currentTarget;

        e.preventDefault();
        e.stopPropagation();

        if (!form.checkValidity()) {
            setValidated(true);
            return;
        }

        const payload = {...formData};

        try {
            setCreateMealPlanRecordCall({state: "pending"});
            await handlerMap.handleCreateMealPlanRecord(payload);
            handleModalClose();
            forgetData()
            setCreateMealPlanRecordCall({state: 'inactive'})
        } catch (e) {
            console.error(e);
            setCreateMealPlanRecordCall({state: "error", error: e.message});
        }
    };

    const forgetData = () => {
        setFormData({meal_plan_id: mealPlanId, food_item_id: "", weight: "",})
    };

    const handleModalClose = () => {
        setValidated(false);
        handleClose();
    };

    const capitalizeFirstLetter = (string) => {
        return string.charAt(0).toUpperCase() + string.slice(1);
    };

    return (<Modal show={show} onHide={handleModalClose}>
        <Form noValidate validated={validated} onSubmit={handleSubmit}>
            <Modal.Header closeButton>
                <Modal.Title>Create Meal Plan Record</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form.Group controlId="formMealPlanRecordFoodItem">
                    <Form.Label>Food Item</Form.Label>
                    <Form.Select
                        name="mealPlanRecordFoodItem"
                        value={formData.food_item_id}
                        onChange={(e) => setField("food_item_id", e.target.value)}
                        required
                    >
                        <option value="">Select a food item</option>

                        {foodItemList.sort(
                            (a,b) =>
                                (a.name.toLowerCase() > b.name.toLowerCase()) ? 1 :
                                    ((b.name.toLowerCase() > a.name.toLowerCase()) ? -1 : 0)
                        ).map((foodItem) => {
                            return (<option key={foodItem.id} value={foodItem.id}>
                                    {capitalizeFirstLetter(foodItem.name)}
                                </option>)
                        })}
                    </Form.Select>
                    <Form.Control.Feedback type="invalid">
                        This field is required
                    </Form.Control.Feedback>
                </Form.Group>
                <Form.Group controlId="formMealPlanRecordWeight">
                    <Form.Label>Weight (g)</Form.Label>
                    <Form.Control
                        type="number"
                        placeholder="Enter meal weight"
                        name="mealPlanRecordWeight"
                        value={formData.weight}
                        onChange={(e) => setField("weight", parseInt(e.target.value))}
                        min={0}
                        step={50}
                        required
                    />
                    <Form.Control.Feedback type="invalid">
                        This field is required
                    </Form.Control.Feedback>
                </Form.Group>
            </Modal.Body>
            <Modal.Footer>
                <div>
                    {createMealPlanRecordCall.state === 'error' &&
                        <div className="text-danger">Error: {createMealPlanRecordCall.error.errorMessage}</div>}
                </div>
                <Button variant="secondary" onClick={handleModalClose}>
                    Close
                </Button>
                <Button variant="primary" type="submit" disabled={createMealPlanRecordCall.state === 'pending'}>
                    {createMealPlanRecordCall.state === 'pending' ? (
                        <Icon size={0.8} path={mdiLoading} spin={true}/>) : ("Submit")}
                </Button>
            </Modal.Footer>
        </Form>
    </Modal>);
}

export default CreateMealPlanRecordModal;
