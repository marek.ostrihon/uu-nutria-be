import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import styles from "../css/Header.module.css";
import Navbar from "react-bootstrap/Navbar";
import Button from "react-bootstrap/Button";
import CreateMealPlanModal from "./CreateMealPlanModal";
import CreateFoodItemModal from "./CreateFoodItemModal";
import {useNavigate} from "react-router-dom";

function Header() {
  const [showCreateFoodItemModal, setShowCreateFoodItemModal] = useState(false);
  const [showCreateMealPlanModal, setShowCreateMealPlanModal] = useState(false);

  const handleCloseCreateFoodItemModal = () => setShowCreateFoodItemModal(false);
  const handleCloseCreateMealPlanModal = () => setShowCreateMealPlanModal(false);

  const navigate = useNavigate();

  return (
    <div className="Header">
      {/*Header*/}
      <Navbar bg="light">
        <div className="container-fluid">
          <Navbar.Brand href="/">Nutria</Navbar.Brand>
        </div>
      </Navbar>

      {/*Buttons*/}
      <div className={styles.buttonContainer}>
        <Button
          className={styles.buttonLeft}
          variant="success"
          onClick={() => setShowCreateMealPlanModal(true)}
        >
          + New Meal Plan
        </Button>
        <Button
          className={styles.buttonRight}
          variant="primary"
          onClick={() => setShowCreateFoodItemModal(true)}
        >
          + New Food Item
        </Button>
      </div>
      <div className={styles.buttonContainer}>
        <Button
          className={styles.buttonRight}
          variant="outline-success"
          onClick={() => navigate('/mealPlanList')}
        >
          Show Meal Plans
        </Button>
      </div>

      {/*Modals*/}
      <CreateMealPlanModal
        show={showCreateMealPlanModal}
        handleClose={handleCloseCreateMealPlanModal}
      />
      <CreateFoodItemModal
        show={showCreateFoodItemModal}
        handleClose={handleCloseCreateFoodItemModal}
      />
    </div>
  );
}

export default Header;
