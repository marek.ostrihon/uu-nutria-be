import React, {useState, useMemo} from "react";
import FoodItemCard from "./FoodItemCard";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Icon from "@mdi/react";
import {mdiMagnify} from "@mdi/js";
import styles from "../css/Styles.module.css";

function FoodItemGrid(props) {
  const [searchBy, setSearchBy] = useState("");

  const filteredFoodItemList = useMemo(() => {
    return props.foodItemList.filter((foodItem) => {
      return foodItem.name.toLocaleLowerCase().includes(searchBy.toLocaleLowerCase());
    });
  }, [searchBy, props.foodItemList]);

  function handleSearch(event) {
    event.preventDefault();
    setSearchBy(event.target["searchInput"].value);
  }

  function handleSearchDelete(event) {
    if (!event.target.value) setSearchBy("");
  }

  return (
      <div>
          {/*Search bar*/}
          <div className={styles.searchContainer}>
              <Form className={styles.searchForm} onSubmit={handleSearch}>
                  <Form.Control
                      id={"searchInput"}
                      className={styles.searchInput}
                      type="search"
                      placeholder="Search"
                      aria-label="Search"
                      onChange={handleSearchDelete}
                  />
                  <Button className={styles.searchButton} variant="outline-success" type="submit">
                      <Icon size={1} path={mdiMagnify}/>
                  </Button>
              </Form>
          </div>

          {/*Food item list*/}
          <div className={styles.gridList}>
              {filteredFoodItemList.map((foodItem) => {
                  return <FoodItemCard key={foodItem.id} foodItem={foodItem}/>;
              })}
          </div>
      </div>
  );
}

export default FoodItemGrid;
