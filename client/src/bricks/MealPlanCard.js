import React, {useState} from "react";
import Card from "react-bootstrap/Card";
import {ListGroup} from "react-bootstrap";
import Button from "react-bootstrap/Button";
import styles from "../css/Styles.module.css";
import CreateMealPlanModal from "./CreateMealPlanModal";
import CreateMealPlanRecordModal from "./CreateMealPlanRecordModal";

function MealPlanCard(props) {

    const [showCreateMealPlanRecordModal, setShowCreateMealPlanRecordModal] = useState(false);

    let caloriesEaten = 0;

    for (const meal_plan_record of props.mealPlan.meal_plan_records) {
        caloriesEaten += meal_plan_record.food_item_energy_value * (meal_plan_record.weight / 100)
    }

    const handleCloseCreateMealPlanRecordModal = () => setShowCreateMealPlanRecordModal(false);

    return (
        <div className={styles.cardContainer}>
            <Button
                className={styles.roundButton}
                onClick={() => setShowCreateMealPlanRecordModal(true)}
            >
                +
            </Button>
            <Card style={{width: '18rem'}} className={"text-center mb-2 " + styles.mealPlanCard}>
                <Card.Body>
                    <Card.Title>Meal Plan</Card.Title>
                    <Card.Text>{props.mealPlan.date}</Card.Text>
                    <Card.Text><b>Calories eaten: {caloriesEaten}</b></Card.Text>
                    <Card.Text>Calorie goal: {props.mealPlan.calorie_goal}</Card.Text>
                </Card.Body>
                <ListGroup className="list-group-flush">

                    {props.mealPlan.meal_plan_records.map((mealPlanRecord) => {
                        return (<ListGroup.Item key={mealPlanRecord.id} className={styles.listGroupItem}>
                            <span>{mealPlanRecord.food_item_name}</span>
                            <span>{mealPlanRecord.weight}g</span>
                        </ListGroup.Item>)
                    })}
                </ListGroup>
            </Card>


            <CreateMealPlanRecordModal
                show={showCreateMealPlanRecordModal}
                handleClose={handleCloseCreateMealPlanRecordModal}
                mealPlanId={props.mealPlan.id}
            />
        </div>
    );
}

export default MealPlanCard;