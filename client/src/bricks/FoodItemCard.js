import React from "react";
import Card from "react-bootstrap/Card";
import styles from "../css/Styles.module.css";

function FoodItemCard(props) {
  return (
    <Card style={{ width: '18rem' }} className={"text-center mb-2 " + styles.foodItemCard} >
      <Card.Body>
        <div style={{textTransform: 'capitalize'}}>
          {props.foodItem.name}
        </div>
        <div style={{textTransform: 'capitalize'}}>
          {props.foodItem.energy_value} kcal/100g
        </div>
      </Card.Body>
    </Card>
  );
}

export default FoodItemCard;