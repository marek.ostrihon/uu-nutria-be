import React from "react";
import MealPlanCard from "./MealPlanCard";
import styles from "../css/Styles.module.css";

function MealPlanGrid(props) {

  return (
      <div>
          {/*Meal plan list*/}
          <div className={styles.gridList}>
              {props.mealPlanList.map((mealPlan) => {
                  return <MealPlanCard key={mealPlan.id} mealPlan={mealPlan}/>;
              })}
          </div>
      </div>
  );
}

export default MealPlanGrid;
