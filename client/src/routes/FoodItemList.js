import React, {useContext, } from "react";
import styles from "../css/Styles.module.css";
import Icon from "@mdi/react";
import {mdiLoading} from "@mdi/js";
import FoodItemGrid from "../bricks/FoodItemGrid";
import FoodItemContext from "../providers/FoodItemProvider";

function FoodItemList(props) {

  const { state, data, error } = useContext(FoodItemContext);

  function getChild() {
  switch (state) {
    case "pending":
      return (
        <div className={styles.loading}>
          <Icon size={2} path={mdiLoading} spin={true} />
        </div>
      );
    case "success":
      return (
          <FoodItemGrid foodItemList={data} />
      );
    case "error":
      return (
        <div className={styles.error}>
          <div>Error when loading data.</div>
          <br />
          <pre>{JSON.stringify(error, null, 2)}</pre>
        </div>
      );
    default:
      return null;
    }
  }

  return <div>{getChild()}</div>;
}

export default FoodItemList;
