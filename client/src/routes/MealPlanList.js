import React, {useContext} from "react";
import styles from "../css/Styles.module.css";
import Icon from "@mdi/react";
import {mdiLoading} from "@mdi/js";
import MealPlanGrid from "../bricks/MealPlanGrid";
import MealPlanContext from "../providers/MealPlanProvider";

function MealPlanList(props) {

  function orderMealPlansByDateDescending(array) {
    return array.sort((a, b) => {
        const dateA = new Date(a.date);
        const dateB = new Date(b.date);
        return dateB - dateA;
    });
}

  const { state, data, error } = useContext(MealPlanContext);


  function getChild() {
  switch (state) {
    case "pending":
      return (
        <div className={styles.loading}>
          <Icon size={2} path={mdiLoading} spin={true} />
        </div>
      );
    case "success":
      const orderedMealPlans = orderMealPlansByDateDescending(data)
      return (
          <MealPlanGrid mealPlanList={orderedMealPlans} />
      );
    case "error":
      return (
        <div className={styles.error}>
          <div>Error when loading data.</div>
          <br />
          <pre>{JSON.stringify(error, null, 2)}</pre>
        </div>
      );
    default:
      return null;
    }
  }

  return <div>{getChild()}</div>;
}

export default MealPlanList;
