import {createContext, useEffect, useState} from "react";

const FoodItemContext = createContext();

export function FoodItemProvider({children}) {
    const [foodItemListLoadCall, setFoodItemListLoadCall] = useState({
        state: "pending",
        error: null,
        data: null,
    });

    useEffect(() => {
        handleLoad()
    }, []);

    async function handleLoad() {
        setFoodItemListLoadCall((current) => ({...current, state: "pending"}));
        const response = await fetch(`http://localhost:3000/foodItem/list`, {
            method: "GET",
        });
        const responseJson = await response.json();

        if (response.status < 400) {
            setFoodItemListLoadCall({state: "success", data: responseJson});
            return responseJson;
        } else {
            setFoodItemListLoadCall((current) => ({
                state: "error", data: current.data, error: responseJson.error,
            }));
        }
    }

  async function handleCreateFoodItem(dtoIn) {
    const response = await fetch(`http://localhost:3000/foodItem/create`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(dtoIn),
    });
    const responseJson = await response.json();

    if (response.status < 400) {
      handleLoad();
    } else {
      setFoodItemListLoadCall((current) => ({
        state: "error",
        data: current.data,
        error: responseJson.error,
      }));
    }
  }


    const value = {
        state: foodItemListLoadCall.state,
        data: foodItemListLoadCall.data,
        error: foodItemListLoadCall.error,
        handlerMap: {handleCreateFoodItem},
    }

    return (<FoodItemContext.Provider
            value={value}
        >
            {children}
        </FoodItemContext.Provider>)
}

export default FoodItemContext;