import {createContext, useEffect, useState} from "react";

const MealPlanContext = createContext();

export function MealPlanProvider({children}) {
    const [mealPlanListLoadCall, setMealPlanListLoadCall] = useState({
        state: "pending",
        error: null,
        data: null,
    });

    useEffect(() => {
        handleLoad()
    }, []);

    async function handleLoad() {
        setMealPlanListLoadCall((current) => ({...current, state: "pending"}));
        const response = await fetch(`http://localhost:3000/mealPlan/list`, {
            method: "GET",
        });
        const responseJson = await response.json();

        if (response.status < 400) {
            setMealPlanListLoadCall({state: "success", data: responseJson});
            return responseJson;
        } else {
            setMealPlanListLoadCall((current) => ({
                state: "error", data: current.data, error: responseJson.error,
            }));
        }
    }

  async function handleCreateMealPlan(dtoIn) {
    const response = await fetch(`http://localhost:3000/mealPlan/create`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(dtoIn),
    });
    const responseJson = await response.json();

    if (response.status < 400) {
      handleLoad();
    } else {
      setMealPlanListLoadCall((current) => ({
        state: "error",
        data: current.data,
        error: responseJson.error,
      }));
    }
  }

  async function handleCreateMealPlanRecord(dtoIn) {
    const response = await fetch(`http://localhost:3000/mealPlanRecord/create`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(dtoIn),
    });
    const responseJson = await response.json();

    if (response.status < 400) {
      handleLoad();
    } else {
      setMealPlanListLoadCall((current) => ({
        state: "error",
        data: current.data,
        error: responseJson.error,
      }));
    }
  }


    const value = {
        state: mealPlanListLoadCall.state,
        data: mealPlanListLoadCall.data,
        error: mealPlanListLoadCall.error,
        handlerMap: {handleCreateMealPlan, handleCreateMealPlanRecord},
    }

    return (<MealPlanContext.Provider
            value={value}
        >
            {children}
        </MealPlanContext.Provider>)
}

export default MealPlanContext;