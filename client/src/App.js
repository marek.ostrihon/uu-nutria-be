import "./css/Header.module.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Header from "./bricks/Header";
import {Outlet} from "react-router-dom";
import React from "react";


function App() {
  return(
    <div>
      <Header/>
      <Outlet />
    </div>
  )
}


export default App;
