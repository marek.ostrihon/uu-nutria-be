import React from 'react';
import ReactDOM from 'react-dom/client';
import {BrowserRouter, Routes, Route} from "react-router-dom";
import './index.css';
import reportWebVitals from './reportWebVitals';
import App from './App';
import FoodItemList from "./routes/FoodItemList";
import MealPlanList from "./routes/MealPlanList";
import {FoodItemProvider} from "./providers/FoodItemProvider";
import {MealPlanProvider} from "./providers/MealPlanProvider";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<React.StrictMode>
    <MealPlanProvider>
        <FoodItemProvider>
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<App/>}>
                        <Route index element={<FoodItemList/>}/>
                        <Route path="mealPlanList" element={<MealPlanList/>}/>
                    </Route>
                </Routes>
            </BrowserRouter>
        </FoodItemProvider>
    </MealPlanProvider>
</React.StrictMode>);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
